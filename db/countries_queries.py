import json
import collections

from config.db.db_config import conn


def format_structure(row: list) -> dict:
            d = collections.OrderedDict()
            d['region'] = row[0]
            d['country'] = row[1]
            d['language'] = row[2]
            d['time'] = row[4]
            return d

def get_countries_data():
    try:
        c = conn.cursor()
        c.execute('select * from countries;')
        data = c.fetchall()
        listado = [format_structure(row) for row in data]
        c.close()
        return json.dumps(listado)

    except Exception as e:
        print(f'--> error retrieving data from db {e}')
        return {}
