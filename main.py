from services.countries import get_country_data, async_country_data
from services.regions import get_regions
from services.dataframe import get_data_frame, process_data_times, create_json_file, feed_database
from db.countries_queries import get_countries_data


def process_data():
    regions = get_regions()
    info_list = async_country_data(regions)
    data = get_data_frame(info_list)
    times = process_data_times(data)
    file_result = create_json_file(data)
    db_result = feed_database(data)

    print(data)
    print(times)
    print(f'--> file creation {file_result}')
    print(f'--> db insertion {db_result}')
    #TODO: do more tests and include coverage
    #TODO: shared volume to acces generated files from host
    return times

def query_database():
    return get_countries_data()
