import sqlite3
import os

env = os.getenv('ENVIRONMENT')

if env == 'test':
    conn = sqlite3.connect('files/countries_data_test.db')
else:
    conn = sqlite3.connect('files/countries_data.db')

c = conn.cursor()

c.execute('CREATE TABLE IF NOT EXISTS countries (region, country_name, language_hash, language, time_formatted, time)')
conn.commit()
c.close()
