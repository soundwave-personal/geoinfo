import json

from wsgiref.simple_server import make_server

from main import process_data, query_database


def router(environ, start_response):
    headers = [('Content-type', 'application/json; charset=utf-8')]
  
    if environ['PATH_INFO'] == '/process_data':
        try:
            status = '200 OK'
            start_response(status, headers)
            
            result = {
                'processing_times': process_data()
            }

            result = json.dumps(result)
            return [result.encode('utf-8')]
        except Exception as e:
            print(f'--> error in /process_data request {e}')
            status = '500 INTERNAL SERVER ERROR'
            start_response(status, headers)
            return [b"{'error': 'server error}"]

    elif environ['PATH_INFO'] == '/query_data':
        try:
            status = '200 OK'
            start_response(status, headers)
            result = query_database()
            return [result.encode('utf-8')] 
        except Exception as e:
            print(f'--> error in /query_data request {e}')
            status = '500 INTERNAL SERVER ERROR'
            start_response(status, headers)
            return [b"{'error': 'server error}"]

    elif environ['PATH_INFO'] == '/':
        status = '200 OK'
        start_response(status, headers)
        return [b"{'hello':'world'}"] 

    else:
        status = '404 NOT FOUND'
        start_response(status, headers)
        return [b"Not found"]
   

with make_server('', 8000, router) as httpd:
    print("Serving on port 8000...")
    httpd.serve_forever()
