============
zinobe test
============

This is the repo to hold the test for zinobe test.


============
how to run?
============

After you clone the repo, you have to create .env and .test.env files to set this variables:

.. code :: bash

    for .env
    export API_REGIONS="https://rapidapi.p.rapidapi.com/all"
    export RAPID_API_HOST="restcountries-v1.p.rapidapi.com"
    export RAPID_API_KEY=your-rapid-api-key
    export API_COUNTRIES="https://restcountries.eu/rest/v2/region/"
    export ENVIRONMENT=dev

    for .test.env
    export API_REGIONS="https://rapidapi.p.rapidapi.com/all"
    export RAPID_API_HOST="restcountries-v1.p.rapidapi.com"
    export RAPID_API_KEY=your-rapid-api-key
    export API_COUNTRIES="https://restcountries.eu/rest/v2/region/"
    export ENVIRONMENT=test



After this, you can execute:

.. code :: bash

    sudo docker-compose up


Note: if you want to run without docker, (I recommend to use python 3.7 and above) run:

.. code :: bash

    pip install requirements.txt
    python api.py


==============
the endpoints
==============

there are two endpoints:

- /process_data [GET] start all the data processing cicle

- /query_data [GET] queries generated sqlite database

*************
/process_data
*************

request and response see like this:

.. image:: https://imagizer.imageshack.com/img923/8538/xVvk7U.png


*************
/query_data
*************

request and response see like this:

.. image:: https://imagizer.imageshack.com/img922/7064/jIbhTR.png



==================
test and coverage
==================

To run tests and see coverage run:

.. code :: bash

    pytest --envfile .test.env --cov


==================
what do I do?
==================

- The process_data endpoint gets data from rapid-api and from that data extracts the regions using a python generator. Then sends the set to a function that sends simultaneous queries to restcoutries api, obtains a random country and forms a dictionary to store in sqlite database and store .json file too. Finally the endpoint returns as response time analysis of the data process.
- The query_data endpoint executes a query to retrieve data stored in database, parses to json dict a shows as the endpoint response.
- db file and json files generated can be seen in project_directory/files

I built this using requests to retrieve data from external apis, and for serve the api I use python pure with wsgi to start a simple server.

Flow representation:

.. image:: https://imagizer.imageshack.com/img922/2737/BLAGi0.png
