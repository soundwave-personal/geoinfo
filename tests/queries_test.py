import os
import pytest

from db.countries_queries import get_countries_data


def test_get_countries_data():
    result = get_countries_data()
    assert type(result) == str