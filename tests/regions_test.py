import pytest
import os

from services.regions import get_regions


def test_get_valid_regions():
    result = get_regions()
    assert type(result) == set
    assert len(result) > 0

# def test_get_invalid_regions_response():
#     TEMP_HOLDER_URL = os.getenv('API_REGIONS')
#     os.environ['API_REGIONS'] = 'INVALID_URL'
#     print(os.getenv('API_REGIONS'))
#     result = get_regions()
#     os.environ['API_REGIONS'] = TEMP_HOLDER_URL
#     assert len(result) == 0
    