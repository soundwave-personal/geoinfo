import pytest

from services.countries import get_country_data


def test_get_valid_countries():
    result = get_country_data('Europe')
    assert type(result) == dict
    assert len(result) > 0

def test_get_invalid_countries():
    result = get_country_data('')
    assert len(result) == 0
