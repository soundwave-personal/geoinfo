import os
import pytest

from pandas import DataFrame

from services.dataframe import get_data_frame, process_data_times, create_json_file, feed_database
from .structure_mocks import countries_list, countries_dataframe


def test_get_data_frame():
    result = get_data_frame(countries_list)
    assert type(result) == DataFrame
    assert 'time' in result.keys()

def test_get_data_frame_fail():
    result = get_data_frame('invalid_data')
    assert len(result) == 0

def test_process_data_times():
    result = process_data_times(countries_dataframe)
    assert type(result) == dict
    assert 'max-time' in result.keys()
    assert 'min-time' in result.keys()
    assert 'average-time' in result.keys()
    assert 'total-time' in result.keys()

def test_proces_data_times_fail():
    result = process_data_times('invalid_data')
    assert type(result) == dict
    assert len(result) == 0

def test_file_generation():
    result = create_json_file(countries_dataframe)
    os.remove('files/data-test.json')
    assert result == True

def test_file_generation_fail():
    result = create_json_file('invalid_data')
    assert result == False

def test_database_feeding():
    result = feed_database(countries_dataframe)
    os.remove('files/countries_data_test.db')
    assert result == True

def test_database_feeding_fail():
    result = feed_database('invalid_data')
    assert result == False
