import pandas as pd

countries_list = [
    {
        'region': 'Europe', 
        'country_name': 'Luxembourg', 
        'language_hash': 'b5b0c37d9d05797b423207e9103e364a6ab7945a', 
        'language': 'French', 
        'time_formatted': '0.42 ms', 
        'time': 0.42
    }, 
    {
        'region': 'Africa', 
        'country_name': 'Réunion', 
        'language_hash': 'b5b0c37d9d05797b423207e9103e364a6ab7945a', 
        'language': 'French', 
        'time_formatted': '0.45 ms', 
        'time': 0.45
    }, 
    {
        'region': 'Polar', 
        'country_name': 'Antarctica', 
        'language_hash': 'b5b0c37d9d05797b423207e9103e364a6ab7945a', 
        'language': 'English', 
        'time_formatted': '0.33 ms', 
        'time': 0.33
    }, 
    {
        'region': 'Asia', 
        'country_name': 'Nepal', 
        'language_hash': 'b5b0c37d9d05797b423207e9103e364a6ab7945a', 
        'language': 'Nepali', 
        'time_formatted': '0.44 ms', 
        'time': 0.44
    }, 
    {
        'region': 'Americas', 
        'country_name': 'Aruba', 
        'language_hash': 'b5b0c37d9d05797b423207e9103e364a6ab7945a', 
        'language': 'Dutch', 
        'time_formatted': '0.38 ms', 
        'time': 0.38
    }, 
    {
        'region': 'Oceania', 
        'country_name': 'Norfolk Island', 
        'language_hash': 'b5b0c37d9d05797b423207e9103e364a6ab7945a', 
        'language': 'English', 
        'time_formatted': '0.4 ms', 
        'time': 0.4
    }
]

countries_dataframe = pd.DataFrame.from_dict(countries_list)
