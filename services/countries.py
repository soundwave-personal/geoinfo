import hashlib
import json
import os
import requests

from time import time
from random import randrange
from concurrent.futures import ThreadPoolExecutor


API_COUNTRIES = os.getenv('API_COUNTRIES')


def get_country_data(region: str) -> dict:
    try:
        start = time()
        response = requests.request("GET", API_COUNTRIES+region)
        json_response = json.loads(response.text)
        random_country = randrange(len(json_response))

        hash_object = hashlib.sha1((json_response[random_country]['languages'][0]['name']).encode('utf-8')).hexdigest()
        
        total_time = round(time()-start, 2)

        info = {
            'region': region,
            'country_name': json_response[random_country]['name'],
            'language_hash': hash_object,
            'language': json_response[random_country]['languages'][0]['name'],
            'time_formatted': f'{total_time} ms',
            'time': total_time
        }

        return info
    except Exception as e:
        print(f"--> there's an error trying to get countries info {e}")
        return {}


def async_country_data(regions: set) -> dict:
    with ThreadPoolExecutor (max_workers=50) as pool:
        info = list(pool.map(get_country_data, regions))
        return info

