import calendar
import os
import pandas as pd
import time

from pandas import DataFrame

from config.db.db_config import conn


env = os.getenv('ENVIRONMENT')


def get_data_frame(info: list) -> DataFrame:
    try:
        data = pd.DataFrame.from_dict(info)
        return data
    except Exception as e:
        print(f'--> error creating dataframe {e}')
        return DataFrame.from_dict({})


def process_data_times(data: DataFrame) -> dict:
    try:
        time_column = data['time']

        total_time = time_column.sum()
        average_time = time_column.mean()
        min_time = time_column.min()
        max_time = time_column.max()
        
        return {
            'total-time': total_time,
            'average-time': average_time,
            'min-time': min_time,
            'max-time': max_time
        }

    except Exception as e:
        print(f'--> error in dataframe times calc: {e}')
        return {}


def create_json_file(data: DataFrame) -> bool:
    try:
        now = calendar.timegm(time.gmtime())
        if env == 'test':
            data.to_json(f'./files/data-test.json')
        else:
            data.to_json(f'./files/data-{now}.json')
        return True
        
    except Exception as e:
        print(f'--> error generating json file {e}')
        return False

def feed_database(data: DataFrame) -> bool:
    try:
        data.to_sql('countries', conn, if_exists='append', index=False)
        return True
    except Exception as e:
        print(f'--> error inserting in db {e}')
        return False

