import json
import os
import requests


API_REGIONS = os.getenv('API_REGIONS')
RAPID_API_HOST = os.getenv('RAPID_API_HOST')
RAPID_API_KEY = os.getenv('RAPID_API_KEY')


def get_regions() -> set:
    headers_regions = {
        'x-rapidapi-host': RAPID_API_HOST,
        'x-rapidapi-key': RAPID_API_KEY
    }
    
    try:
        response = requests.request("GET", API_REGIONS, headers=headers_regions)
        countries = json.loads(response.text)
        return {country['region'] for country in countries if country['region']}
    except Exception as e:
        print(f"--> there's an error trying to get regions info {e}")
        return set()
